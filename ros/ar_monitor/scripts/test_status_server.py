#!/usr/bin/env python

import unittest
import rospy
from parser_help import LaunchFileParser
from ar_monitor.srv import statusInfo,statusInfoResponse,statusInfoRequest
from ar_monitor.msg import Node, Navdata
from time import sleep
from rosgraph_msgs.msg import Log
import rostest
from status_server import server_callback, getRequiredNodeNames


class TestStatusServer(unittest.TestCase):

    talker_ok = False
    resp1 = None

    def callback(self, data):
        self.talker_ok = True

    def test_call_service_with_good_input_string(self):
        rospy.wait_for_service('ar_status_service')
        try:
            ar = rospy.ServiceProxy('ar_status_service', statusInfo)
            self.resp1 = ar('ar_monitor/all.launch')

        except rospy.ServiceException:
            print("Service call failed")

        self.assertNotEqual(None, self.resp1)

    def test_call_service_with_bad_input_string(self):
        rospy.wait_for_service('ar_status_service')
        try:
            ar = rospy.ServiceProxy('ar_status_service', statusInfo)
            self.resp1 = ar('ardrone_autonomy')

        except rospy.ServiceException:
            print("Service call failed")

        self.assertEqual(None, self.resp1)

    def test_call_service_with_NONE_input(self):
        rospy.wait_for_service('ar_status_service')
        try:
            ar = rospy.ServiceProxy('ar_status_service', statusInfo)
            self.resp1 = ar(None)

        except rospy.ServiceException:
            print("Service call failed")

        self.assertEqual(None, self.resp1)

    def test_subscribe_to_rosout(self):
        rospy.init_node('test_status_server')
        rospy.Subscriber('/rosout', Log, self.callback)

        counter = 0

        while not rospy.is_shutdown() and counter < 5 and (not self.talker_ok):
            sleep(1)
            counter += 1
            self.talker_ok = True

        self.assertEqual(True,self.talker_ok)

    def test_status_server_server_callback(self):
        statusInfo.arg = 'ar_monitor/all.launch'
        respond = server_callback(statusInfo)
        self.assertNotEqual(None, respond)

    def test_status_server_getRequiredNodeNames(self):

        parser = LaunchFileParser('ar_monitor', 'all.launch')
        nodes = dict()
        parser.parse()
        nodes = parser.get_nodes()
        response = getRequiredNodeNames(nodes)
        self.assertNotEqual(None, response)

    def test_status_server_server_callback_wrong_input(self):
        statusInfo.arg = ' / '
        response = ''
        try:
            respond = server_callback(statusInfo)
        except Exception:
            print('Not found the launch file')
            respond = None

        self.assertEqual(None, respond)

    def test_parser_help_parse(self):
        parser = LaunchFileParser('ar_monitor', 'all.launch')
        self.assertNotEqual(None, parser)



if __name__ == '__main__':

    rostest.rosrun('ar_monitor', 'test_status_server', TestStatusServer)
    print('Tests for status_server.py')

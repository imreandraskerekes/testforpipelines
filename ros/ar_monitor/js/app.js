var ros=new ROSLIB.Ros();
var port=null;
var serverHostname=null;
var selectedTopic=null;
var selectedNode=null;	
var selectedRequiredNode=null;
var status=0;

var topics=[];
var nodes=[];
var required_nodes=[];
var params=[];
var test=[];


var app=angular.module("monitorApp",[]);

app.controller("userCtrl",function($scope)
{

	$scope.updateServerAddress=function()
	{
		$scope.serverAddress="ws://"+$scope.serverHostname;
		$scope.serverAddress+=":"+$scope.port;
	}

	$scope.getStatus=function()
	{
			if($scope.status==0)
			{
				return false;
			}else
			{
				return true;
			}
	}

	$scope.showStatus=function()
	{
		if($scope.status==0)
		{
			return "Disconnected";
		}else
		{
			return "Connected";
		}
	}

	$scope.clear=function()
	{
		$scope.serverHostname="";
		$scope.port="";
	}

	$scope.connect=function()
	{
		var temp_stat=-1;
		$scope.updateServerAddress()

		$scope.ros.connect($scope.serverAddress);

		$scope.ros.on('connection',function()
		{
			console.log("A connection has been established!");
			temp_stat=1;
		});


		$scope.ros.on('error',function(error)
		{
			alert("An error has occured while connecting!");
			temp_stat=0;
		});

		$scope.ros.on('close',function()
		{
			console.log("The connection has been closed!");
			temp_stat=0;
		
		});

		$scope.status=temp_stat;
	}

	$scope.disconnect=function()
	{
		$scope.disconnect=function()
		{
			$scope.ros.close();
			$scope.status=0;
		}
	}

	$scope.test=function()
	{
		$scope.showTopics=$scope.showTopics===false ? true:false;

		console.log($scope.showTopics);
	}

	$scope.change = function () {
        console.log($scope.newItemType)
    };

    $scope.topicClass=function()
    {
    	return $scope.showTopics ? 'glyphicon glyphicon-minus':'glyphicon glyphicon-plus';
    }	

    $scope.nodeClass=function()
    {
    	return $scope.showNodes ? 'glyphicon glyphicon-minus' : 'glyphicon glyphicon-plus';
    }

    $scope.nodeReqClass=function()
    {
    	return $scope.showReqNodes ? 'glyphicon glyphicon-minus' : 'glyphicon glyphicon-plus';
    }

    $scope.topicIconChange=function()
    {
    	$scope.showTopics=$scope.showTopics ? false : true;
    }

    $scope.nodeIconChange=function()
    {
    	$scope.showNodes=$scope.showNodes ? false : true;
    }

    $scope.nodeReqIconChange=function()
    {
    	$scope.showReqNodes=$scope.showReqNodes ? false : true;
    }

    $scope.selectTopic=function(topic)
    {
    	$scope.selectedTopic=topic;
    }

    $scope.selectNode=function(node)
    {
    	$scope.selectedNode=node;
    }

    $scope.selectRequiredNode=function(node)
    {
    	$scope.selectedRequiredNode=node;
    }

    $scope.nodeNumber=function()
    {
    	return $scope.nodes.length;
    }

    $scope.topicNumber=function()
    {
    	return $scope.topics.length;
    }
   
    $scope.getTopicsReq=function()
    {
    	var client=new ROSLIB.Service({
    		ros:$scope.ros,
    		name:'/ar_status_service',
    		serviceType:'ar_monitor/statusInfo'
    	});

    	var request=new ROSLIB.ServiceRequest(
    				{
    					action:0
    				});

    	$scope.topics.length=0;

   		client.callService(request,function(result)
   		{
   			for(i=0;i<result.topics.length;i++)
   			{
   				var temp_topic={
   					name:result.topics[i],
   					datatype:result.topics_type[i]
   				};
   				
   				$scope.topics.push(temp_topic);
   			}
   		});

    }
			
    $scope.getTopicsAPI=function()
	{
			//topics=[];

			//$scope.topics.splice(0,$scope.topics.length);
			//$scope.topics=[];

			$scope.ros.getTopics(function(aliveTopics)
			{

				for(i=0;i<aliveTopics.topics.length;i++)
				{
					var temp_topic={
						name:aliveTopics.topics[i],
						datatype:aliveTopics.types[i]
					};

					topics.push(temp_topic);
				}

				$scope.topics.push(temp_topic);
				
			});
			$scope.topics=topics;
			$scope.topicNumber();
	}

	$scope.getNodesAPI=function()
	{
		nodes=[];

		$scope.ros.getNodes(function(aliveNodes)
		{
			angular.forEach(aliveNodes,function(node)
			{
				var temp_node={
					name:node
				};

				nodes.push(temp_node);
			});
		});

		$scope.nodes=nodes;
		$scope.nodeNumber();
	}

	$scope.getNodesReq=function()
	{
		var client=new ROSLIB.Service({
			ros:$scope.ros,
			name:'/ar_status_service',
			serviceType:'ar_monitor/statusInfo'
		});
	
		$scope.nodes.length=0;

		var request=new ROSLIB.ServiceRequest({
			action:0
		});

		client.callService(request,function(result)
		{
			for(i=0;i<result.nodes.length;i++)
			{
				var temp_node={
					name:result.nodes[i]
				};

				$scope.nodes.push(temp_node);
			}
		});
	}

	$scope.pingNode=function(node)
	{
		var client=new ROSLIB.Service({
			ros:$scope.ros,
			name:'/ar_status_service',
			serviceType:'ar_monitor/statusInfo'

		});

		var request=new ROSLIB.ServiceRequest({
			action:3,
			node_name:node.name
		});

		client.callService(request,function(result)
		{
			if(!result.node_status)
			{
				alert(node.name+' is not alive!');
			}else{
				alert(node.name+' is alive!');
			}
		});

	}


	$scope.killNode=function(node)
	{
		alert('Killing node :'+node.name);

		var client=new ROSLIB.Service({
			ros:$scope.ros,
			name:'/ar_status_service',
			serviceType:'ar_monitor/statusInfo'
		});

		var request=new ROSLIB.ServiceRequest(
		{
			action:1,
			node_name:node.name
		});


		client.callService(request,function(result)
		{
				
		});

		$scope.removeNode(node);
	}

	$scope.startNode=function(node)
	{
		var client=new ROSLIB.Service({
			ros:$scope.ros,
			name:'/ar_status_service/',
			serviceType:'ar_monitor/statusInfo'
		});

		var request=new ROSLIB.ServiceRequest({
			action:2,
			node_name:node.name,
			node_type:node.type,
			pkg_name:node.pkg,
			args_name:node.args
		});

		client.callService(request,function(result)
		{

		});

		$scope.getNodesReq();
	}

	$scope.getRequiredNodes=function()
	{
		var client=new ROSLIB.Service({
			ros:$scope.ros,
			name:'/ar_status_service',
			serviceType:'ar_monitor/statusInfo'
		});

		var request=new ROSLIB.ServiceRequest({
			action:4
		});

		$scope.required_nodes.length=0;

		client.callService(request,function(result)
		{

			for(i=0;i<result.required_nodes.length;i++)
			{
				var temp_req_node={
					name:result.required_nodes[i].name.data,
					pkg:result.required_nodes[i].pkg.data,
					type:result.required_nodes[i].type.data,
					args:result.required_nodes[i].args.data,
					running:result.required_nodes[i].running.data,
					demoDomain:result.required_nodes[i].demo_domain.data
				}

				$scope.required_nodes.push(temp_req_node);
			}
		});
	}

	$scope.removeNode=function(node)
	{
		var index=-1;

		for(i=0;i<nodes.length;i++)
		{
			if("/"+node.name===nodes[i].name)
			{
				index=i;
			}
		}

		nodes.splice(index,1);
	}

	$scope.nodeRunningClass=function(node)
	{

		if(node.running==true)
		{
			return "success";
		}

		return "danger";

		/*for(i=0;i<nodes.length;i++)
		{
			if("/"+node.name===nodes[i].name)
			{
				return "success";
			}
		}
		return "danger";*/
	}

	$scope.getNumberOfTopics=function()
	{
		var count = 0;
		angular.forEach($scope.topics,function(item)
		{
			count++;
		});

		console.log(count);
		return count;
	}


	$scope.testTopics=function()
	{
		angular.forEach($scope.topics,function(item)
		{
			console.log(item.name+' '+item.datatype);
		});
	}

	
	$scope.ros=ros;
	$scope.port=port;
	$scope.serverHostname=serverHostname;
	$scope.status=status;
	$scope.showTopics=false;
	$scope.showNodes=false;
	$scope.topics=topics;
	$scope.nodes=nodes;
	$scope.required_nodes=required_nodes;
	$scope.params=params;
	$scope.test=test;
	$scope.selectedTopic=selectedTopic;
	$scope.selectedNode=selectedNode;
	$scope.selectedRequiredNode=selectedRequiredNode;

});
